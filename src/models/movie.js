const { model, Schema } = require('mongoose')

const movieSchema = new Schema(
	{
		title: {
			type: String,
			required: true
		},
		year: {
			type: Number,
			required: true
		},
		genres: [
			{
				type: String,
				required: true
			}
		],
		ratings: [
			{
				type: Number,
				required: true
			}
		],
		duration: String,
		releaseDate: {
			type: Date,
			required: true
		},
		averageRaiting: Number,
		storyline: String,
		actors: [
			{
				type: String,
				required: true
			}
		],
		imdbRaiting: Number,
		posterurl: String
	},
	{
		timestamps: true
	}
)

module.exports = model('Movie', movieSchema)
