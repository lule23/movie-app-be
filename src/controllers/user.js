const User = require('../models/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const login = async (req, res, next) => {
	const { email, password } = req.body

	try {
		const user = await User.findOne({ email })
		if (!user) {
			throw new Error('User not found!')
		}

		const hashedPassword = await bcrypt.compare(
			password,
			user.password
		)
		if (!hashedPassword) {
			throw new Error('Wrong user or password!')
		}

		const token = await jwt.sign({ user: user._id }, 'secret', {
			expiresIn: '1h'
		})

		res.json({
			message: 'Success',
			token
		})
	} catch (error) {
		throw new Error(error)
	}
}

const signup = async (req, res, next) => {
	const { email, password, firstName, lastName } = req.body

	try {
		const user = await User.findOne({ email })
		if (user) {
			throw new Error('User already registerd!')
		}

		const hashedPassword = await bcrypt.hash(password, 12)
		if (!hashedPassword) {
			throw new Error('Something went wrong!')
		}

		const newUser = new User({
			email,
			password: hashedPassword,
			firstName,
			lastName
		})

		await newUser.save()

		res.json({
			message: 'Success'
		})

		console.log(user)
	} catch (error) {
		throw new Error(error)
	}
}

const getInfo = async (req, res, next) => {
	const id = req.userId

	try {
		const user = await User.findById(id)
		if (!user) {
			throw new Error('User not found!')
		}

		res.json({
			data: {
				firstName: user.firstName,
				lastName: user.lastName
			}
		})
	} catch (error) {
		throw new Error(error)
	}
}

module.exports = {
	login,
	signup,
	getInfo
}
