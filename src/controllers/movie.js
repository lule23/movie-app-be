const Movie = require('../models/movie')

const info = async (req, res, next) => {
	try {
		const { id } = req.params
		let movie
		if (id) {
			movie = await Movie.findById(id)
		} else {
			movie = await Movie.find()
		}

		if (!movie) {
			throw new Error('Movie not found!')
		}

		res.status(200).json({
			movie
		})
	} catch (error) {
		throw new Error(error)
	}
}

const create = async (req, res, next) => {
	try {
		const movie = await Movie.findOne({ title: req.body.title })
		if (movie) {
			throw new Error('Movie already exist!')
		}

		const newMovie = new Movie({ ...req.body })
		await newMovie.save()

		res.status(200).json({
			newMovie
		})
	} catch (error) {
		throw new Error(error)
	}
}

const edit = async (req, res, next) => {
	const { id } = req.params

	try {
		const movie = await Movie.findById(id)
		if (!movie) {
			throw new Error('Movie not found!')
		}

		await Movie.findByIdAndUpdate(id, req.body)

		console.log(req.body)

		res.status(200).json({
			message: 'Movie edited successfuly!'
		})
	} catch (error) {
		throw new Error(error)
	}
}

module.exports = {
	info,
	create,
	edit
}
