const mongoose = require('mongoose')
const Movie = require('../models/movie')
const User = require('../models/user')

const data = require('../../data.json')

mongoose.connect('mongodb://localhost:27017/netflix').then(() => {
	const createMovies = async () => {
		await Movie.remove({})

		data.forEach(async movie => {
			const newmovie = new Movie({ ...movie })
			await newmovie.save()
		})

		const admin = new User({
			firstName: 'John',
			lastName: 'Doe',
			email: 'example@mail.com',
			password: '1234567',
			location: {
				city: 'Belgrade',
				country: 'Serbia',
				address: 'Timočka 11'
			}
		})

		await admin.save()

		process.exit()
	}

	createMovies()
})
