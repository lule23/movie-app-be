const express = require('express')
const router = express.Router()
const isAuth = require('../middleware/auth')

const movieController = require('../controllers/movie')

router.post('/edit/:id', isAuth, movieController.edit)
router.post('/create', isAuth, movieController.create)
router.delete('/delete', isAuth)

router.get('/info', movieController.info)
router.get('/info/:id', movieController.info)

module.exports = router
