const express = require('express')
const router = express.Router()
const isAuth = require('../middleware/auth')

const userController = require('../controllers/user')

router.post('/login', userController.login)
router.post('/signup', userController.signup)

router.get('/info', isAuth, userController.getInfo)

module.exports = router
