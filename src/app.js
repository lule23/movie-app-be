const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

const userRoutes = require('./routes/user')
const movieRoutes = require('./routes/movies')

const app = express()
app.use(cors())
app.use(express.json())

app.use('/user', userRoutes)
app.use('/movie', movieRoutes)

app.use((error, req, res, next) => {
	const status = 500
	const message = error.message

	res.status(status).json({ message: message })
})

mongoose
	.connect('mongodb://localhost:27017/netflix', {
		useNewUrlParser: true
	})
	.then(() => {
		app.listen(3001, () => {
			console.log('runing on port 3001')
		})
	})
	.catch(err => console.log(err))
